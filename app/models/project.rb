class Project < ApplicationRecord
  has_many :tasks, dependent: :destroy

  validates :name, presence: true

  def incomplete_tasks
    tasks.reject(&:complete?)
  end

  def done?
    tasks.all?(&:complete?)
  end

  def total_size
    tasks.sum(&:size)
  end

  def remaining_size
    incomplete_tasks.sum(&:size)
  end

  def completed_velocity
    tasks.sum(&:points_towards_velocity)
  end

  def current_rate
    completed_velocity * 1.0/Project.velocity_length_in_days
  end

  def expected_days_remaining
    remaining_size / current_rate
  end

  def on_schedule?
    return false if expected_days_remaining.nan?
    (Time.zone.today) + expected_days_remaining <= due_date
  end

  def self.velocity_length_in_days
    21
  end
end